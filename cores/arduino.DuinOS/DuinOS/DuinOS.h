/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"

/* Priority definitions for most of the tasks in the demo application.  Some
tasks just use the idle priority. */
#define LOW_PRIORITY		(tskIDLE_PRIORITY)
#define NORMAL_PRIORITY		(tskIDLE_PRIORITY + 1)
#define HIGH_PRIORITY		(tskIDLE_PRIORITY + 2)
#define TOP_PRIORITY		(tskIDLE_PRIORITY + 3)

