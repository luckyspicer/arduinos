/*
    FreeRTOS V7.1.1 - Copyright (C) 2012 Real Time Engineers Ltd.
	

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

#include <avr/io.h>

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE. 
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/

//##2009.10.29: These values may be improved:
#if defined(__AVR_ATmega644__) || defined(__AVR_ATmega644P__)

	//##Multiplo.Brain.M644:
	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 18432000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these big cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	//##Run experiments to test this value:
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 2048 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		1
	#define INCLUDE_uxTaskPriorityGet		1
	//##If the following value is set to 1, change the memory managment scheme to heap_2.c:
	#define INCLUDE_vTaskDelete				0
	#define INCLUDE_vTaskCleanUpResources	0
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1

#elif defined(__AVR_ATmega1284P__)

	//##Multiplo.Brain.M1284
	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 18432000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these big cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	//##Run experiments to test this value:
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 4096 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		1
	#define INCLUDE_uxTaskPriorityGet		1
	//##If the following value is set to 1, change the memory managment scheme to heap_2.c:
	#define INCLUDE_vTaskDelete				0
	#define INCLUDE_vTaskCleanUpResources	0
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1

#elif defined(__AVR_ATmega1280__)
	//##Arduino Mega? Not tested yet:
	//#define portUSE_TIMER0
	//#define portUSE_TIMER1
	#define portUSE_TIMER3
	
	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 16000000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these bigger cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	//##Run experiments to test this value:
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 4096 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		1
	#define INCLUDE_uxTaskPriorityGet		1
	//##If the following value is set to 1, change the memory managment scheme to heap_2.c:
	#define INCLUDE_vTaskDelete				0
	#define INCLUDE_vTaskCleanUpResources	0
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1
	
#elif defined(__AVR_ATmega2560__)
	//##Arduino Mega 2560? Not tested yet:
	//#define portUSE_TIMER0
	//#define portUSE_TIMER1
	#define portUSE_TIMER3
	
	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 16000000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these bigger cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	//##Run experiments to test this value:
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 4096 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		1
	#define INCLUDE_uxTaskPriorityGet		1
	//##If the following value is set to 1, change the memory managment scheme to heap_2.c:
	#define INCLUDE_vTaskDelete				0
	#define INCLUDE_vTaskCleanUpResources	0
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1	

#elif defined(__AVR_ATmega328P__)
	//##Mega328p:

	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 12000000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these bigger cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 1200 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		0
	#define INCLUDE_uxTaskPriorityGet		0
	#define INCLUDE_vTaskDelete				1
	#define INCLUDE_vTaskCleanUpResources	1
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1

#elif defined(__AVR_ATmega88__) || defined(__AVR_ATmega88P__) || defined(__AVR_ATmega168__) || defined(__AVR_ATmega168P__)
	//##Mega88, Mega88p, Mega168, Mega168p:

	#define configUSE_PREEMPTION		1
	#define configUSE_IDLE_HOOK			0
	#define configUSE_TICK_HOOK			0

	//##20091029: Use compiler defined freq.:
	#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) F_CPU )
	//#define configCPU_CLOCK_HZ			( ( unsigned portLONG ) 12000000 )

	#define configTICK_RATE_HZ			( ( portTickType ) 1000 )
	//##For these bigger cpus, it's possible to define more priorities if necessary:
	#define configMAX_PRIORITIES		( ( unsigned portBASE_TYPE ) 3 )
	#define configMINIMAL_STACK_SIZE	( ( unsigned portSHORT ) 85 )
	//20101220 : decrease heap size from 800 to 500 - see http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1256745982/46#46
	#define configTOTAL_HEAP_SIZE		( (size_t ) ( 500 ) )
	//#define configMAX_TASK_NAME_LEN		( 8 )
	#define configMAX_TASK_NAME_LEN		( 16 )
	#define configUSE_TRACE_FACILITY	0
	#define configUSE_16_BIT_TICKS		1
	#define configIDLE_SHOULD_YIELD		0
	#define configQUEUE_REGISTRY_SIZE	0

	/* Co-routine definitions. */
	//##2009.10.20: defined as "0":
	#define configUSE_CO_ROUTINES 		0
	#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )

	/* Set the following definitions to 1 to include the API function, or zero
	to exclude the API function. */
	#define INCLUDE_vTaskPrioritySet		0
	#define INCLUDE_uxTaskPriorityGet		0
	#define INCLUDE_vTaskDelete				0
	#define INCLUDE_vTaskCleanUpResources	0
	#define INCLUDE_vTaskSuspend			1
	#define INCLUDE_vTaskDelayUntil			1
	#define INCLUDE_vTaskDelay				1

#else
	#error "Device is not supported by DuinOS"

#endif

#if !defined(DUINOS_USE_HEAP1) && ! defined(DUINOS_USE_HEAP2) && !defined(DUINOS_USE_HEAP3)
  #define DUINOS_USE_HEAP1 1
#endif


#endif /* FREERTOS_CONFIG_H */
